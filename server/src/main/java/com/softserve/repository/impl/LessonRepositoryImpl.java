package com.softserve.repository.impl;

import com.softserve.entity.Lesson;
import com.softserve.repository.LessonRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class LessonRepositoryImpl extends BasicRepositoryImpl<Lesson, Long> implements LessonRepository {
    private static final String  NOT_DISABLED_SQL = " and l.teacher.disable=false and l.subject.disable=false and l.group.disable=false";

    /**
     * Method gets information about all lessons for particular group from DB
     *
     * @param groupId Identity number of the group for which need to find all lessons
     * @return List of filtered lessons
     */
    @Override
    public List<Lesson> getAllForGroup(Long groupId) {
        log.info("In getAllForGroup(groupId = [{}])", groupId);
        return sessionFactory.getCurrentSession().createQuery
                ("FROM Lesson l WHERE l.group.id = :groupId " + NOT_DISABLED_SQL)
                .setParameter("groupId", groupId).getResultList();
    }

    /**
     * Method searches duplicate of lesson in the DB
     *
     * @param lesson Lesson entity that needs to be verified
     * @return count of duplicates if such exist, else return 0
     */
    @Override
    public Long countLessonDuplicates(Lesson lesson) {
        log.info("In countLessonDuplicates(lesson = [{}])", lesson);
        return (Long) sessionFactory.getCurrentSession().createQuery("" +
                "select count(*) from Lesson l " +
                "where l.teacher.id = :teacherId " +
                "and l.subject.id = :subjectId " +
                "and l.group.id = :groupId " +
                "and l.lessonType = :lessonType " +
                "and l.teacher.disable=false and l.subject.disable=false and l.group.disable=false")
                .setParameter("teacherId", lesson.getTeacher().getId())
                .setParameter("subjectId", lesson.getSubject().getId())
                .setParameter("groupId", lesson.getGroup().getId())
                .setParameter("lessonType", lesson.getLessonType())
                .getSingleResult();
    }

    // Checking if lesson is used in Schedule table
    @Override
    protected boolean checkReference(Lesson lesson) {
        log.info("In checkReference(lesson = [{}])", lesson);
        long count = (long) sessionFactory.getCurrentSession().createQuery
                ("select count (s.id) " +
                        "from Schedule s where s.lesson.id = :lessonId")
                .setParameter("lessonId", lesson.getId())
                .getSingleResult();
        return count != 0;
    }

    /**
     * Method gets information about all lessons from DB
     *
     * @return List of all lessons
     */
    @Override
    public List<Lesson> getAll() {
        log.info("In getAll()");
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lesson.class, "l");
        criteria.createAlias("l.teacher", "teacher");
        criteria.createAlias("l.subject", "subject");
        criteria.createAlias("l.group", "group");
        criteria.add(Restrictions.eq("teacher.disable",false));
        criteria.add(Restrictions.eq("subject.disable",false));
        criteria.add(Restrictions.eq("group.disable",false));
        List<Lesson> results = criteria.list();
        return results;
    }
}
