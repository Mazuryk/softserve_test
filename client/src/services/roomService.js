import { store } from '../index';
import { reset } from 'redux-form';
import { DISABLED_ROOMS_URL, ROOM_URL } from '../constants/axios';
import { ROOM_FORM } from '../constants/reduxForms';
import axios from '../helper/axios';

import {
    showListOfRooms,
    deleteRoom,
    addRoom,
    selectOneRoom,
    updateOneRoom,
    clearRoomOne,
    setDisabledRooms
} from '../redux/actions/rooms';

import { handleSnackbarOpenService } from './snackbarService';
import { snackbarTypes } from '../constants/snackbarTypes';

import i18n from '../helper/i18n';

const resetFormHandler = form => {
    store.dispatch(reset(form));
};

export const showListOfRoomsService = () => {
    axios
        .get(ROOM_URL)
        .then(response => {
            let bufferArray = [];
            const results = response.data;
            for (const key in results) {
                bufferArray.push({
                    id: key,
                    ...results[key]
                });
            }
            store.dispatch(showListOfRooms(bufferArray));
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response
                    ? i18n.t(
                    err.response.data.message,
                    err.response.data.message
                    )
                    : err
            );
        });
};

export const deleteRoomCardService = id => {
    axios
        .delete(ROOM_URL + `/${id}`)
        .then(res => {
            store.dispatch(deleteRoom(id));
            getDisabledRoomsService();
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:back_end_success_operation', {
                    cardType: i18n.t('formElements:room_label'),
                    actionType: i18n.t('serviceMessages:deleted_label')
                })
            );
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response
                    ? i18n.t(
                    err.response.data.message,
                    err.response.data.message
                    )
                    : err
            );
        });
};

export const getDisabledRoomsService = () => {
    axios
        .get(DISABLED_ROOMS_URL)
        .then(res => {
            store.dispatch(setDisabledRooms(res.data));
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response
                    ? i18n.t(
                    err.response.data.message,
                    err.response.data.message
                    )
                    : err
            );
        });
};

export const setDisabledRoomsService = room => {
    room.disable = true;
    put(room);
};

export const setEnabledRoomsService = room => {
    room.disable = false;
    put(room);
};

const put = values => {
    axios
        .put(ROOM_URL, values)
        .then(result => {
            store.dispatch(updateOneRoom(result.data));
            resetFormHandler(ROOM_FORM);
            getDisabledRoomsService();
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:back_end_success_operation', {
                    cardType: i18n.t('formElements:room_label'),
                    actionType: i18n.t('serviceMessages:updated_label')
                })
            );
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response
                    ? i18n.t(
                    err.response.data.message,
                    err.response.data.message
                    )
                    : err
            );
        });
};
const post = values => {
    axios
        .post(ROOM_URL, values)
        .then(res => {
            store.dispatch(addRoom(res.data));
            resetFormHandler(ROOM_FORM);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:back_end_success_operation', {
                    cardType: i18n.t('formElements:room_label'),
                    actionType: i18n.t('serviceMessages:created_label')
                })
            );
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response
                    ? i18n.t(
                    err.response.data.message,
                    err.response.data.message
                    )
                    : err
            );
        });
};

export const createRoomService = values => {
    if (values.id) {
        const newValue = {
            id: values.id,
            name: values.name,
            type: { id: +values.type, description: values.typeDescription }
        };
        put(newValue);
    } else {
        const newValue = {
            name: values.name,
            type: { id: +values.type, description: values.typeDescription }
        };

        post(newValue);
    }
};

export const selectOneRoomService = roomId => {
    store.dispatch(selectOneRoom(roomId));
};

export const clearRoomOneService = () => {
    store.dispatch(clearRoomOne());
    resetFormHandler(ROOM_FORM);
};
