import { store } from '../index';
import { reset } from 'redux-form';
import { ROOM_FORM_TYPE } from '../constants/reduxForms';
import { ROOM_TYPES_URL } from '../constants/axios';
import axios from '../helper/axios';
import { cardType } from '../constants/cardType';

import { getAllRoomTypes, deleteType, updateOneType, postOneType, getOneNewType } from '../redux/actions/roomTypes';

import { handleSnackbarOpenService } from './snackbarService';
import { snackbarTypes } from '../constants/snackbarTypes';
import { snackbarMessage } from '../constants/snackbarMessages';
import { actionType } from '../constants/actionTypes';

import i18n from '../helper/i18n';
import { errorHandler } from '../helper/handlerAxios';

const resetFormHandler = form => {
    store.dispatch(reset(form));
};

export const getAllRoomTypesService = () => {
    axios
        .get(ROOM_TYPES_URL)
        .then(res => {
            store.dispatch(getAllRoomTypes(res.data));
        })
        .catch(error => {
            errorHandler(error);
        });
};

export const deleteTypeService = roomTypeId => {
    axios
        .delete(ROOM_TYPES_URL + `/${roomTypeId}`)
        .then(response => {
            store.dispatch(deleteType(roomTypeId));
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.TYPE, actionType.DELETED)
            );
        })
        .catch(error => {
            errorHandler(error);
        });
};

export const addNewTypeService = values => {
    if (values.id) {
        putNewType(values);
    } else {
        postNewType(values);
    }
};

export const putNewType = values => {
    axios
        .put(ROOM_TYPES_URL, values)
        .then(response => {
            store.dispatch(updateOneType(response.data));
            resetFormHandler(ROOM_FORM_TYPE);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:back_end_success_operation', {
                    cardType: i18n.t('formElements:type_label'),
                    actionType: i18n.t('serviceMessages:updated_label')
                })
            );
        })
        .catch(error => {
            errorHandler(error);
        });
};

export const postNewType = values => {
    axios
        .post(ROOM_TYPES_URL, values)
        .then(response => {
            store.dispatch(postOneType(response.data));
            resetFormHandler(ROOM_FORM_TYPE);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:back_end_success_operation', {
                    cardType: i18n.t('formElements:type_label'),
                    actionType: i18n.t('serviceMessages:created_label')
                })
            );
        })
        .catch(error => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                i18n.t(error.response.data.message, error.response.data.message)
            );
        });
};

export const getOneNewTypeService = roomId => {
    store.dispatch(getOneNewType(roomId));
}


