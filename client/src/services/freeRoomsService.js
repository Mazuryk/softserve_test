import axios from '../helper/axios';
import { store } from '../index';
import { reset } from 'redux-form';

import { showFreeRooms, clearFreeRooms } from '../redux/actions/freeRooms';
import { FREE_ROOMS_URL } from '../constants/axios';
import { FREE_ROOMS } from '../constants/reduxForms';
import { handleSnackbarOpenService } from './snackbarService';
import { snackbarTypes } from '../constants/snackbarTypes';

import i18n from '../helper/i18n';

const resetFormHandler = formName => {
    store.dispatch(reset(formName));
};

export const showFreeRoomsService = elem => {
    axios
        .get(
            FREE_ROOMS_URL +
                '?dayOfWeek=' +
                elem.dayOfWeek +
                '&evenOdd=' +
                elem.evenOdd +
                '&classId=' +
                elem.class +
                '&semesterId=' +
                elem.semesterId
        )
        .then(response => {
            let bufferArray = [];
            const results = response.data;
            for (const key in results) {
                bufferArray.push({
                    id: key,
                    ...results[key]
                });
            }
            store.dispatch(showFreeRooms(bufferArray));
        })
        .catch(error => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                i18n.t(error.response.data.message, error.response.data.message)
            );
        });
};

export const clearFreeRoomsService = () => {
    store.dispatch(clearFreeRooms());
    resetFormHandler(FREE_ROOMS);
};
