import { reset } from 'redux-form';

import { store } from '../index';
import axios from '../helper/axios';
import {
    DISABLED_GROUPS_URL,
    DISABLED_ROOMS_URL,
    GROUP_URL
} from '../constants/axios';
import { cardType } from '../constants/cardType';
import { GROUP_FORM } from '../constants/reduxForms';
import { actionType } from '../constants/actionTypes';
import { snackbarTypes } from '../constants/snackbarTypes';
import { handleSnackbarOpenService } from './snackbarService';
import { snackbarMessage } from '../constants/snackbarMessages';
import {
    showAllGroups,
    deleteGroup,
    addGroup,
    selectGroup,
    updateGroup,
    clearGroup,
    setDisabledRooms,
    setDisabledGroups
} from '../redux/actions/index';
import i18n from '../helper/i18n';

const resetForm = form => store.dispatch(reset(form));

export const selectGroupService = groupId =>
    store.dispatch(selectGroup(groupId));

export const handleGroupService = values =>
    values.id ? updateGroupService(values) : createGroupService(values);

export const clearGroupService = () => {
    store.dispatch(clearGroup());
    resetForm(GROUP_FORM);
};

export const showAllGroupsService = () => {
    axios
        .get(GROUP_URL)
        .then(response => {
            store.dispatch(showAllGroups(response.data.sort((a, b) => a - b)));
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const removeGroupCardService = groupId => {
    axios
        .delete(GROUP_URL + `/${groupId}`)
        .then(response => {
            store.dispatch(deleteGroup(groupId));
            getDisabledGroupsService();
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.GROUP, actionType.DELETED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const createGroupService = data => {
    axios
        .post(GROUP_URL, data)
        .then(response => {
            store.dispatch(addGroup(response.data));
            resetForm(GROUP_FORM);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.GROUP, actionType.CREATED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const updateGroupService = data => {
    return axios
        .put(GROUP_URL, data)
        .then(response => {
            store.dispatch(updateGroup(response.data));
            selectGroupService(null);
            getDisabledGroupsService();
            resetForm(GROUP_FORM);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.GROUP, actionType.UPDATED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const getDisabledGroupsService = () => {
    axios
        .get(DISABLED_GROUPS_URL)
        .then(res => {
            store.dispatch(setDisabledGroups(res.data));
        })
        .catch(error => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                i18n.t(error.response.data.message, error.response.data.message)
            );
        });
};

export const setDisabledGroupService = group => {
    group.disable = true;
    updateGroupService(group);
};

export const setEnabledGroupService = group => {
    group.disable = false;
    updateGroupService(group);
};
