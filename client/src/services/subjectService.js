import { reset } from 'redux-form';

import { store } from '../index';
import axios from '../helper/axios';
import { DISABLED_SUBJECTS_URL, SUBJECT_URL } from '../constants/axios';
import { cardType } from '../constants/cardType';
import { actionType } from '../constants/actionTypes';
import { SUBJECT_FORM } from '../constants/reduxForms';
import { snackbarTypes } from '../constants/snackbarTypes';
import { handleSnackbarOpenService } from './snackbarService';
import { snackbarMessage } from '../constants/snackbarMessages';
import {
    addSubject,
    clearSubject,
    deleteSubject,
    selectSubject,
    setDisabledSubjects,
    showAllSubjects,
    updateSubject
} from '../redux/actions/index';
import i18n from '../helper/i18n';

const resetForm = form => store.dispatch(reset(form));

export const selectSubjectService = subjectId =>
    store.dispatch(selectSubject(subjectId));

export const handleSubjectService = values =>
    values.id ? updateSubjectService(values) : createSubjectService(values);

export const clearSubjectService = () => {
    store.dispatch(clearSubject());
    resetForm(SUBJECT_FORM);
};

export const showAllSubjectsService = () => {
    axios
        .get(SUBJECT_URL)
        .then(response => {
            store.dispatch(showAllSubjects(response.data));
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const removeSubjectCardService = subjectId => {
    axios
        .delete(SUBJECT_URL + `/${subjectId}`)
        .then(response => {
            store.dispatch(deleteSubject(subjectId));
            getDisabledSubjectsService();
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.SUBJECT, actionType.DELETED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const createSubjectService = data => {
    axios
        .post(SUBJECT_URL, data)
        .then(response => {
            store.dispatch(addSubject(response.data));
            resetForm(SUBJECT_FORM);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.SUBJECT, actionType.CREATED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const updateSubjectService = data => {
    return axios
        .put(SUBJECT_URL, data)
        .then(response => {
            store.dispatch(updateSubject(response.data));
            selectSubjectService(null);
            showAllSubjectsService();
            getDisabledSubjectsService();
            resetForm(SUBJECT_FORM);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                snackbarMessage(cardType.SUBJECT, actionType.UPDATED)
            );
        })
        .catch(error =>
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                `${error.response.data.message}`
            )
        );
};

export const getDisabledSubjectsService = () => {
    axios
        .get(DISABLED_SUBJECTS_URL)
        .then(res => {
            store.dispatch(setDisabledSubjects(res.data));
        })
        .catch(error => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                i18n.t(error.response.data.message, error.response.data.message)
            );
        });
};

export const setDisabledSubjectsService = subject => {
    subject.disable = true;
    updateSubjectService(subject);
};

export const setEnabledSubjectsService = subject => {
    subject.disable = false;
    updateSubjectService(subject);
};
