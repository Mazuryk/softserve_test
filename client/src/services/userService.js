import axios from '../helper/axios';
import { store } from '../index';

import { MERGE_USER_AND_TEACHER_URL, USERS_URL } from '../constants/axios';

import { handleSnackbarOpenService } from './snackbarService';

import { setUsers } from '../redux/actions/index';

import { snackbarTypes } from '../constants/snackbarTypes';
import i18n from '../helper/i18n';
import { setLoadingService } from './loadingService';
import { getTeachersWithoutAccount } from './teacherService';

export const getUsersService = () => {
    axios
        .get(USERS_URL)
        .then(response => {
            store.dispatch(setUsers(response.data));
        })
        .catch(err => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response.data.message
            );
        });
};

export const mergeUserAndTeacherService = mergeObj => {
    axios
        .put(MERGE_USER_AND_TEACHER_URL, mergeObj)
        .then(response => {
            getTeachersWithoutAccount();
            getUsersService();
            setLoadingService(false);
            handleSnackbarOpenService(
                true,
                snackbarTypes.SUCCESS,
                i18n.t('serviceMessages:successfully_merged')
            );
        })
        .catch(err => {
            setLoadingService(false);
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                err.response.data.message
            );
        });
};
