import { store } from '../index';

import { BUSY_ROOMS } from '../constants/axios';
import { handleSnackbarOpenService } from './snackbarService';
import { snackbarTypes } from '../constants/snackbarTypes';
import i18n from '../helper/i18n';

import axios from '../helper/axios';

import { showAllBusyRooms } from '../redux/actions';

export const showBusyRooms = semesterId => {
    axios
        .get(`${BUSY_ROOMS}?semesterId=${semesterId}`)
        .then(response => {
            store.dispatch(showAllBusyRooms(response.data));
        })
        .catch(error => {
            handleSnackbarOpenService(
                true,
                snackbarTypes.ERROR,
                error.response
                    ? i18n.t(
                          error.response.data.message,
                          error.response.data.message
                      )
                    : error
            );
        });
};
