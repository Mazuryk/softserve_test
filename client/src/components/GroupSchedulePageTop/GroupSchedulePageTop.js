import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import Button from '@material-ui/core/Button';
import { MdPlayArrow } from 'react-icons/md';

import {
    setScheduleTypeService,
    setScheduleGroupIdService,
    setScheduleTeacherIdService,
    setScheduleSemesterIdService,
    showAllPublicSemestersService,
    showAllPublicGroupsService,
    showAllPublicTeachersService
} from '../../services/scheduleService';

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { styled } from '@material-ui/core/styles';
import './GroupSchedulePageTop.scss';
import { setLoadingService } from '../../services/loadingService';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1)
        }
    }
}));

const GroupField = styled(TextField)({
    display: 'inline-block',
    width: '150px'
});

let groupId = 0;
let teacherId = 0;
let semesterId = 0;

const GroupSchedulePageTop = props => {
    const classes = useStyles();
    const { t } = useTranslation('common');
    const { groups, teachers, semesters } = props;
    const isLoading = props.loading;

    let loadingContainer = '';
    if (isLoading) {
        loadingContainer = (
            <section className="centered-container">
                <CircularProgress />
            </section>
        );
    }

    useEffect(() => showAllPublicGroupsService(), []);
    useEffect(() => showAllPublicTeachersService(), []);
    useEffect(() => showAllPublicSemestersService(), []);

    const defaultProps = {
        options: groups,
        getOptionLabel: option => (option ? option.title : '')
    };

    const defaultTeacherProps = {
        options: teachers,
        getOptionLabel: option =>
            option
                ? option.surname + ' ' + option.name + ' ' + option.patronymic
                : ''
    };

    const defaultSemesterProps = {
        options: semesters,
        getOptionLabel: option => (option ? option.description : '')
    };
    const handleGroupSelect = group => {
        if (group) {
            groupId = group.id;
        }
    };
    const handleTeacherSelect = teacher => {
        if (teacher) {
            teacherId = teacher.id;
        }
    };
    const handleSemesterSelect = semester => {
        if (semester) {
            semesterId = semester.id;
        }
    };

    const renderSemesterList = () => {
        if (semesters) {
            if (semesters.length > 1) {
                return (
                    <Autocomplete
                        {...defaultSemesterProps}
                        id="semester"
                        name="semester"
                        clearOnEscape
                        openOnFocus
                        onChange={(event, newValue) => {
                            handleSemesterSelect(newValue);
                        }}
                        renderInput={params => (
                            <GroupField
                                {...params}
                                label={t('formElements:semester_label')}
                                margin="normal"
                            />
                        )}
                    />
                );
            } else {
                semesterId = semesters[0].id;
                return <p>{semesters[0].description}</p>;
            }
        }
    };
    return (
        <section className={classes.root}>
            <p>{t('greetings_schedule_message')}</p>
            <p>{t('greetings_schedule_message_hint')}</p>
            <section className="form-buttons-container">
                <div className="schedule-selector-part">
                    {renderSemesterList()}
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            setLoadingService('true');
                            setScheduleTypeService('full');
                            setScheduleSemesterIdService(semesterId);
                        }}
                    >
                        <MdPlayArrow
                            title={t('full_schedule_label')}
                            className="svg-btn"
                        />
                    </Button>
                </div>
                <div className="schedule-selector-part">
                    <Autocomplete
                        {...defaultProps}
                        id="group"
                        name="group"
                        clearOnEscape
                        openOnFocus
                        onChange={(event, newValue) => {
                            handleGroupSelect(newValue);
                        }}
                        renderInput={params => (
                            <GroupField
                                {...params}
                                label={t('formElements:group_label')}
                                margin="normal"
                            />
                        )}
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            if (!semesterId) {
                                alert(t('semester_is_empty'));
                                return;
                            }
                            setLoadingService('true');
                            setScheduleTypeService('group');
                            setScheduleSemesterIdService(semesterId);
                            setScheduleGroupIdService(groupId);
                        }}
                    >
                        <MdPlayArrow
                            title={t('group_schedule_label')}
                            className="svg-btn"
                        />
                    </Button>
                </div>
                <div className="schedule-selector-part">
                    <Autocomplete
                        {...defaultTeacherProps}
                        id="teacher"
                        clearOnEscape
                        openOnFocus
                        onChange={(event, newValue) => {
                            handleTeacherSelect(newValue);
                        }}
                        renderInput={params => (
                            <GroupField
                                {...params}
                                label={t('formElements:teacher_label')}
                                margin="normal"
                            />
                        )}
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            if (!semesterId) {
                                alert(t('semester_is_empty'));
                                return;
                            }
                            setLoadingService('true');
                            setScheduleTypeService('teacher');
                            setScheduleSemesterIdService(semesterId);
                            setScheduleTeacherIdService(teacherId);
                        }}
                    >
                        <MdPlayArrow
                            title={t('teacher_schedule_label')}
                            className="svg-btn"
                        />
                    </Button>
                </div>
            </section>
            {loadingContainer}
        </section>
    );
};

const mapStateToProps = state => ({
    groups: state.groups.groups,
    teachers: state.teachers.teachers,
    semesters: state.schedule.semesters,
    loading: state.loadingIndicator.loading
});
export default connect(mapStateToProps)(GroupSchedulePageTop);
