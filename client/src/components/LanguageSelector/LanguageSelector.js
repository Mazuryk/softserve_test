import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useTranslation } from 'react-i18next';

const LanguageSelector = props => {
    const { t, i18n } = useTranslation();

    const changeLanguage = event => {
        i18n.changeLanguage(event.target.value);
    };

    return (
        <RadioGroup row aria-label="lang" name="lang" value={i18n.language}>
            <FormControlLabel
                control={
                    <>
                        <Radio
                            color="primary"
                            value="en"
                            onChange={changeLanguage}
                        />
                        <img
                            className="language-icon"
                            src="https://image.flaticon.com/icons/svg/555/555417.svg"
                            alt="en"
                        />
                    </>
                }
                label=""
            />
            <FormControlLabel
                control={
                    <>
                        <Radio
                            color="primary"
                            value="uk"
                            onChange={changeLanguage}
                        />
                        <img
                            className="language-icon"
                            src="https://image.flaticon.com/icons/svg/321/321267.svg"
                            alt="en"
                        />
                    </>
                }
                label=""
            />
        </RadioGroup>
    );
};

export default LanguageSelector;
