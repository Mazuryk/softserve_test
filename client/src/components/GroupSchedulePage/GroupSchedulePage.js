import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';

import './GroupSchedulePage.scss';

import {
    makeGroupSchedule,
    makeFullSchedule,
    makeTeacherSchedule
} from '../../helper/prepareSchedule';
import {
    renderGroupTable,
    renderFullSchedule,
    renderWeekTable
} from '../../helper/renderScheduleTable';
import {
    getGroupSchedule,
    getFullSchedule,
    getTeacherSchedule
} from '../../services/scheduleService';

import GroupSchedulePageTop from '../GroupSchedulePageTop/GroupSchedulePageTop';
import { setLoadingService } from '../../services/loadingService';

const GroupSchedulePage = props => {
    let { groupSchedule, fullSchedule, teacherSchedule } = props;
    const matchFunction = scheduleType => {
        switch (scheduleType) {
            case 'full':
                getFullSchedule(props.semesterId);
                break;
            case 'group':
                getGroupSchedule(props.groupId, props.semesterId);
                break;
            case 'teacher':
                getTeacherSchedule(props.teacherId, props.semesterId);
                break;
            default:
                break;
        }
        return 1;
    };
    useEffect(() => {
        matchFunction(props.scheduleType);
    }, [props.scheduleType, props.groupId, props.teacherId, props.semesterId]);

    const emptySchedule = () => (
        <>
            <GroupSchedulePageTop />
            <p className="empty_schedule">{t('common:empty_schedule')}</p>
        </>
    );
    const { t } = useTranslation('common');

    switch (props.scheduleType) {
        case 'group':
            if (
                (!groupSchedule ||
                    (groupSchedule.schedule &&
                        groupSchedule.schedule.length === 0)) &&
                !props.loading
            ) {
                return emptySchedule();
            }
            const resultArrays = makeGroupSchedule(groupSchedule);
            if (resultArrays.done) {
                setLoadingService(false);
                return (
                    <>
                        <GroupSchedulePageTop />
                        <h1>{resultArrays.group.title}</h1>
                        <h2>{t('common:odd_week')}</h2>
                        {renderGroupTable(
                            resultArrays.oddArray,
                            1,
                            resultArrays.semester
                        )}
                        <h2>{t('common:even_week')}</h2>
                        {renderGroupTable(
                            resultArrays.evenArray,
                            0,
                            resultArrays.semester
                        )}
                    </>
                );
            }
            return (
                <>
                    <GroupSchedulePageTop />
                </>
            );

        case 'teacher':
            if (
                (!teacherSchedule ||
                    !teacherSchedule.days ||
                    teacherSchedule.days.length === 0) &&
                !props.loading
            ) {
                return emptySchedule();
            }
            const teacher = makeTeacherSchedule(teacherSchedule);
            if (teacher.done) {
                setLoadingService(false);
                return (
                    <>
                        <GroupSchedulePageTop />
                        <h1>
                            {teacher.teacher.position +
                                ' ' +
                                teacher.teacher.surname +
                                ' ' +
                                teacher.teacher.name +
                                ' ' +
                                teacher.teacher.patronymic}
                        </h1>
                        <h2>{t('common:odd_week')}</h2>
                        {renderWeekTable(teacher.odd, 1)}
                        <h2>{t('common:even_week')}</h2>
                        {renderWeekTable(teacher.even, 0)}
                    </>
                );
            }
            return (
                <>
                    <GroupSchedulePageTop />
                </>
            );

        case 'full':
            if (
                (!fullSchedule.schedule ||
                    fullSchedule.schedule.length === 0) &&
                !props.loading
            ) {
                return emptySchedule();
            }
            const result = makeFullSchedule(fullSchedule);
            if (result.groupsCount || result.done) {
                setLoadingService(false);
                return (
                    <>
                        <GroupSchedulePageTop />
                        {renderFullSchedule(result)}
                    </>
                );
            }
            return (
                <>
                    <GroupSchedulePageTop />
                </>
            );
        default:
            return <GroupSchedulePageTop />;
    }
};
const mapStateToProps = state => ({
    scheduleType: state.schedule.scheduleType,
    groupSchedule: state.schedule.groupSchedule,
    fullSchedule: state.schedule.fullSchedule,
    teacherSchedule: state.schedule.teacherSchedule,
    groupId: state.schedule.scheduleGroupId,
    teacherId: state.schedule.scheduleTeacherId,
    semesterId: state.schedule.scheduleSemesterId,
    loading: state.loadingIndicator.loading
});
export default connect(mapStateToProps)(GroupSchedulePage);
