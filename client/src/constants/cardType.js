export const cardType = {
    LESSON: 'Lesson',
    TEACHER: 'Teacher',
    CLASS: 'Class',
    GROUP: 'Group',
    ROOM: 'Room',
    TYPE: 'Type',
    SUBJECT: 'Subject',
    WISH: 'Wish',
    SEMESTER: 'Semester'
};
