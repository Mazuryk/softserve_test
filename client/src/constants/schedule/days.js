export const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
];

export const daysUppercase = [];
days.map(day => daysUppercase.push(day.toUpperCase()));
