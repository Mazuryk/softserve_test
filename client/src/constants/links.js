export const links = {
    HOME_PAGE: '/',
    ADMIN_PAGE: '/admin',
    SCHEDULE_PAGE: '/schedule',
    AUTH: '/login',
    ACTIVATION_PAGE: '/activation-page',
    LOGOUT: '/logout',
    WISHES: '/wishes'
};
