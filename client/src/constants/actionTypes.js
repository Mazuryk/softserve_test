export const actionType = {
  CREATED: 'created',
  DELETED: 'deleted',
  UPDATED: 'updated'
};
