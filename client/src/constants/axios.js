export const CLASS_URL = 'classes';
export const GROUP_URL = 'groups';
export const LESSON_URL = 'lessons';
export const LESSON_TYPES_URL = 'lessons/types';
export const LOGIN_URL = 'auth/sign-in';
export const LOGOUT_URL = 'auth/sign-out';
export const REGISTRATION_URL = 'auth/sign-up';
export const RESET_PASSWORD_URL = 'auth/reset-password';
export const ACTIVATE_ACCOUNT_URL = 'auth/activation-account';
export const TEACHER_URL = 'teachers';
export const ROOM_URL = 'rooms';
export const ROOM_TYPES_URL = 'room-types';
export const FREE_ROOMS_URL = 'rooms/free';
export const SUBJECT_URL = 'subjects';
export const FULL_SCHEDULE_URL = 'schedules/full/semester?semesterId=';
export const GROUP_SCHEDULE_URL = 'schedules/full/groups?semesterId=';
export const TEACHER_WISHES = 'teacher_wishes';
export const SCHEDULE_SEMESTER_ITEMS_URL = 'schedules/semester';
export const SCHEDULE_ITEMS_URL = 'schedules';
export const CURRENT_SEMESTER_URL = 'semesters/current';

export const SCHEDULE_CHECK_AVAILABILITY_URL = 'schedules/data-before';
export const BUSY_ROOMS = 'schedules/full/rooms';
export const SEMESTERS_URL = 'semesters';
export const TEACHER_SCHEDULE_URL = 'schedules/full/teachers?semesterId=';

export const DISABLED_ROOMS_URL = 'rooms/disabled';
export const DISABLED_TEACHERS_URL = 'teachers/disabled';
export const DISABLED_GROUPS_URL = 'groups/disabled';
export const DISABLED_SEMESTERS_URL = 'semesters/disabled';
export const DISABLED_SUBJECTS_URL = 'subjects/disabled';

export const USERS_URL = 'users/with-role-user';
export const TEACHERS_WITHOUT_ACCOUNT_URL = 'not-registered-teachers';
export const MERGE_USER_AND_TEACHER_URL = 'managers/teacher_credentials';

export const MY_TEACHER_WISHES_URL = 'teacher_wishes/my-wishes';

export const PUBLIC_CLASSES_URL = 'public/classes';
export const PUBLIC_GROUP_URL = 'public/groups';
export const PUBLIC_TEACHER_URL = 'public/teachers';
export const PUBLIC_SEMESTERS_URL = 'public/semesters';
